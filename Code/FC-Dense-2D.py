import densenet
import keras

from medpy.io import load, save
import matplotlib.pyplot as plt
import numpy as np
from os import listdir
from os.path import isfile, join
import os
import tensorflow as tf
from scipy import stats
import scipy
import time
import pandas as pd
from datetime import timedelta
from keras.models import load_model
from scipy.ndimage.morphology import binary_fill_holes

dataPath = '../InputData/'
modelPath = '../Model/'
resultPath = '../Result/'

import os
os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
os.environ["CUDA_VISIBLE_DEVICES"]="0"

batch_size = 3

width = 256
height = 256

n_channels = 1
n_classes = 2

totalTime = []
times = []

### LOSS Function ################

def dice(y_true, y_pred):
    intersection = tf.reduce_sum(tf.multiply(y_pred,y_true))
    return (2. * intersection + 1) / (tf.reduce_sum(y_pred) + tf.reduce_sum(y_true) + 1)

def tversky_coef(y_true, y_pred, alpha, beta):
    y_true_f = K.flatten(y_true)
    y_true_f_r = K.flatten(1. - y_true)
    y_pred_f = K.flatten(y_pred)
    y_pred_f_r = K.flatten(1. - y_pred)
    
    intersection = K.sum(y_true_f * y_pred_f)
    
    fp = K.sum(y_pred_f * y_true_f_r)
    fn = K.sum(y_true_f * y_pred_f_r)

    return intersection / (intersection + alpha * fp + beta * fn)

def tversky_loss(alpha, beta):
    def tversky(y_true, y_pred):
        return -tversky_coef(y_true, y_pred, alpha, beta)
    return tversky

tversky = tversky_loss(alpha=0.3, beta=0.7)

#################################################

# Select largest connected component

def MakeSet(x):
    x.parent = x
    x.rank   = 0
    x.size = 1
    x.sizeOfSet = 0

def Union(x, y):
    xRoot = Find(x)
    yRoot = Find(y)
    if xRoot.rank > yRoot.rank:
        yRoot.parent = xRoot
        xRoot.size += yRoot.size

    elif xRoot.rank < yRoot.rank:
        xRoot.parent = yRoot
        yRoot.size += xRoot.size

    elif xRoot != yRoot:  # Unless x and y are already in same set, merge them
        yRoot.parent = xRoot
        xRoot.rank = xRoot.rank + 1
        xRoot.size += yRoot.size

def Find(x):
    if x.parent == x:
        return x
    else:
        x.parent = Find(x.parent)
        return x.parent

class Node:
    def __init__ (self, label):
        self.label = label
    def __str__(self):
        return self.label

################# Predict Masks #########################

for counter_loss, loss_kind in enumerate(['crossentropy']):
    
    step = 0
    inputCounter = 0
    inputCounter2 = 0
    model_path = modelPath + "Dense_2D_Fetal_Raein_"+loss_kind+".hd5"
    model = load_model(model_path, custom_objects={'dice': dice, 'tversky': tversky})
    counter = 0

    for f2 in listdir(dataPath):

	    print f2
	    image_data, image_header = load(dataPath+f2)
	    #image_data_labels, image_header = load(dataPath+'mask'+f2)
	    imageDim = np.shape(image_data)

	    temp = np.zeros((imageDim[2],width,height,n_channels))
	    tempL = np.zeros((imageDim[2],width,height,n_classes))
	    temp1 = np.zeros_like((imageDim[2],width,height))  
	    temp1 = np.swapaxes(image_data,0,2)
	    temp1 = np.swapaxes(temp1,1,2)
	    
	    ############### ADDED ####################
	    temp1 = temp1 * 1. / np.mean(temp1)
	    
	    print np.shape(temp1)
	    
	    if np.shape(temp1)[1] != width or np.shape(temp1)[2] != height:
		print "bad dimensions"
		continue

	    temp[:,:,:,0] = temp1 * 1.

	    Pmask = np.zeros((imageDim[2],width,height))
	    ProbRes = np.zeros((imageDim[2],width,height))
	    
	    t = time.time()
	    for z in xrange(imageDim[2]):
		if z % batch_size == 0:
		    if z == imageDim[2]-2:
		        image_batch2 = np.zeros((3,width,height,n_channels), dtype=np.float32)
		        image_batch2[0] = temp[z-1]
		        image_batch2[1] = temp[z]
		        image_batch2[2] = temp[z+1]

		        out = model.predict(image_batch2, batch_size=batch_size, verbose=0)

		        _out = np.reshape(out, (3, width, height, n_classes)) 
		        resArr = np.asarray(_out)

		        ########## ADDED ####################
		        _out[:,:,:,0] = _out[:,:,:,0] * 1.
	    
		        output_image = np.argmax(_out, axis=3)
		        Pmask[z-1] = 1-output_image[0]
		        Pmask[z] = 1-output_image[1]
		        Pmask[z+1] = 1-output_image[2]
		        ProbRes[z-1] = resArr[0,:,:,0]
		        ProbRes[z] = resArr[1,:,:,0]
		        ProbRes[z+1] = resArr[2,:,:,0]
		    elif z == imageDim[2]-1:
		        image_batch2 = np.zeros((3,width,height,n_channels), dtype=np.float32)
		        image_batch2[0] = temp[z-2]
		        image_batch2[1] = temp[z-1]
		        image_batch2[2] = temp[z]
		        out = model.predict(image_batch2, batch_size=batch_size, verbose=0)
		        _out = np.reshape(out, (3, width, height, n_classes)) 
		        resArr = np.asarray(_out)

		        ########## ADDED ####################
		        _out[:,:,:,0] = _out[:,:,:,0] * 1.
	    
		        output_image = np.argmax(_out, axis=3)
		        Pmask[z-2] = 1-output_image[0]
		        Pmask[z-1] = 1-output_image[1]
		        Pmask[z] = 1-output_image[2]
		        ProbRes[z-2] = resArr[0,:,:,0]
		        ProbRes[z-1] = resArr[1,:,:,0]
		        ProbRes[z] = resArr[2,:,:,0]
		    else:
		        image_batch2 = np.zeros((3,width,height,n_channels), dtype=np.float32)
		        image_batch2[0] = temp[z]
		        image_batch2[1] = temp[z+1]
		        image_batch2[2] = temp[z+2]
		        out = model.predict(image_batch2, batch_size=batch_size, verbose=0)
		        _out = np.reshape(out, (3, width, height, n_classes))
		        resArr = np.asarray(_out)               

		        ########## ADDED ####################3
		        _out[:,:,:,0] = _out[:,:,:,0] * 1.
		             
		        output_image = np.argmax(_out, axis=3)
		        Pmask[z] = 1 - output_image[0]
		        Pmask[z+1] = 1 - output_image[1]
		        Pmask[z+2] = 1 - output_image[2]
		        ProbRes[z] = resArr[0,:,:,0] 
		        ProbRes[z+1] = resArr[1,:,:,0]
		        ProbRes[z+2] = resArr[2,:,:,0]

	    rows = Pmask
                
	    setDict = {}
	    for i, row in enumerate(rows):
	        for j, val in enumerate(row):
	            for k, index in enumerate(val):
	                if val[k] == 0:
	                    continue
	                node = Node((i, j, k))
    	                MakeSet(node)
	                if i > 0:
	                    if rows[i-1][j][k] == 1:
	                        disjointSet = setDict[(i-1, j, k)]
	                        Union(disjointSet, node)
	                if j > 0:
	                    if row[j-1][k] == 1:
	                        disjointSet = setDict[(i, j-1, k)]
	                        Union(disjointSet, node)
	                if k > 0:
	                    if val[k-1] == 1:
	                        disjointSet = setDict[(i, j, k-1)]
	                        Union(disjointSet, node)
	                if i > 0 and j > 0:
	                    if rows[i-1][j-1][k] == 1:
	                        disjointSet = setDict[(i-1, j-1, k)]
	                        Union(disjointSet, node)
	                if i > 0 and k > 0:
	                    if rows[i-1][j][k-1] == 1:
	                        disjointSet = setDict[(i-1, j, k-1)]
	                        Union(disjointSet, node)
	                if j > 0 and k > 0:
	                    if rows[i][j-1][k-1] == 1:
	                        disjointSet = setDict[(i, j-1, k-1)]
	                        Union(disjointSet, node)
	                if i > 0 and j > 0 and k > 0:
	                    if rows[i-1][j-1][k-1] == 1:
	                        disjointSet = setDict[(i-1, j-1, k-1)]
	                        Union(disjointSet, node)

	                setDict[(i, j, k)] = node

	    maxSize = max([l.size for l in setDict.values()])

	    #print maxSize

	    for l in setDict.values():
	        l.sizeOfSet = Find(l).size
	        if(l.sizeOfSet == maxSize):
	            rows[np.asarray(l.label)[0]][np.asarray(l.label)[1]][np.asarray(l.label)[2]] = 2.

	    Pmask = rows

	    Pmask = stats.threshold(Pmask, threshmin=2., threshmax=2., newval=0.)
	    Pmask = stats.threshold(Pmask, threshmin=0., threshmax=0., newval=1.)
	
	    Pmask = binary_fill_holes(Pmask).astype(int)

	    Pmask = 1. * Pmask

	    temp3 = np.swapaxes(Pmask,1,2)
	    temp3 = np.swapaxes(temp3,0,2)

	    elapsed = time.time() - t
	    times.append(elapsed)
	    save(temp3,resultPath+'DenseNet_2D_Fetal_Raein_mask_'+loss_kind+'_'+f2,image_header)

