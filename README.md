# Fetal Brain Extraction
Our FC-Densenet method for automatic real-time fetal brain extraction.

### What is this repository for? ###

* This is the Python code for testing our FC-Densenet model in real-time Fetal brain segmentation.
* Version 1.0
* IEEE Access paper (2018)
	* https://ieeexplore.ieee.org/document/8573779
	* https://arxiv.org/abs/1803.11078

### Dependencies ###

* Tensorflow (CPU or GPU - GPU is 20 times faster) - https://www.tensorflow.org
* MedPy - https://pypi.python.org/pypi/MedPy
* Python 2.7 - https://www.python.org/download/releases/2.7/

### How to run ###

Create folders the same as the repository:

- Code
- InputData
- Model
- Result

Then, put corresponding files in each folder and run the "FC-Dense-2D.py" python code in the "Code" directory from your terminal.

### Contact ###

* Raein Hashemi (hashemi.s@husky.neu.edu)
* Ali Gholipour (Ali.Gholipour@childrens.harvard.edu)
